package de.awacademy.blog.message;

import de.awacademy.blog.Comments.Comment;
import de.awacademy.blog.Comments.CommentDTO;
import de.awacademy.blog.Comments.CommentRepository;
import de.awacademy.blog.User.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.time.Instant;

@Controller
public class MessageController {

    private MessageRepository messageRepository;
    private CommentRepository commentRepository;

    @Autowired
    public MessageController(MessageRepository messageRepository, CommentRepository commentRepository) {
        this.messageRepository = messageRepository;
        this.commentRepository = commentRepository;
    }

    @GetMapping("/message")
    public String message(Model model) {
        model.addAttribute("message", new MessageDTO("", ""));
        return "message";
    }

    @PostMapping("/message")
    public String message(@Valid @ModelAttribute("message") MessageDTO messageDTO, BindingResult bindingResult, @ModelAttribute("sessionUser") User sessionUser) {
        if (bindingResult.hasErrors()) {
            return "message";
        }
        if (sessionUser != null && sessionUser.getAdmin() == 1) {
            Message message = new Message(sessionUser, messageDTO.getText(), Instant.now(), messageDTO.getTitle());
            messageRepository.save(message);

            return "redirect:/";
        }
        return "message";
    }

    @GetMapping("/message/{messageid}")
    public String commenting(@PathVariable long messageid, Model model) {

        Message message = messageRepository.findById(messageid);
        model.addAttribute("message", message);

        model.addAttribute("commentListe", commentRepository.findAllByMessage(message));
        model.addAttribute("comment", new CommentDTO(""));


        return "comment";
    }


    @PostMapping("/message/{messageid}")
    public String commenting(@PathVariable long messageid, @Valid @ModelAttribute("comment") CommentDTO commentDTO, @ModelAttribute("sessionUser") User sessionUser, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {

            return "comment";
        }
        if (sessionUser != null) {
            Comment comment = new Comment(commentDTO.getText(), Instant.now(), messageRepository.findMessageById(messageid).get(), sessionUser);
            commentRepository.save(comment);

            return "redirect:/";
        }
        return "comment";
    }

    @PostMapping("/profile/{commentid}/delete")
    public String delete(@PathVariable long commentid, @ModelAttribute("sessionUser") User sessionUser) {

        if (sessionUser != null && sessionUser.getAdmin() == 1) {


            Comment comment = commentRepository.findById(commentid).get();
            commentRepository.delete(comment);

            return "redirect:/";
        }
        return "edit";
    }

    @GetMapping("/message/{messageid}/edit")
    public String editMessage(@PathVariable long messageid, Model model) {

        Message message = messageRepository.findMessageById(messageid).get();
        MessageDTO messageDTO = new MessageDTO(message.getTitle(), message.getText());
        messageDTO.setId(messageid);
        model.addAttribute("messageDTO", messageDTO);


        return "edit";
    }


    @PostMapping("/message/{messageid}/edit")
    public String edit(@PathVariable long messageid, @Valid @ModelAttribute("message") MessageDTO messageDTO, @ModelAttribute("sessionUser") User sessionUser, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {

            return "edit";
        }

        if (sessionUser != null && sessionUser.getAdmin() == 1) {


            Message message = messageRepository.findMessageById(messageid).get();
            message.setText(messageDTO.getText());
            message.setTitle(messageDTO.getTitle());
            messageRepository.save(message);

            return "redirect:/";
        }
        return "edit";
    }

    @PostMapping("/profile/{messageid}/messageDelete")
    public String messageDelete(@PathVariable long messageid, @ModelAttribute("sessionUser") User sessionUser) {
        if (sessionUser != null && sessionUser.getAdmin() == 1) {


            Message message = messageRepository.findMessageById(messageid).get();

            messageRepository.delete(message);


            return "redirect:/";

        }
        return "edit";
    }
}
