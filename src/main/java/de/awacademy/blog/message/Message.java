package de.awacademy.blog.message;

import de.awacademy.blog.Comments.Comment;
import de.awacademy.blog.User.User;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Entity
public class Message {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "message" ,cascade = CascadeType.ALL )
    private List<Comment> comments;

    private String text;

    private Instant postedAt;

    private String title;

    public Message() {
    }

    public Message(User user, String text, Instant postedAt, String title) {
        this.user = user;
        this.text = text;
        this.postedAt = postedAt;
        this.title = title;
    }

    public User getUser() {
        return user;
    }

    public String getText() {
        return text;
    }

    public long getId() {
        return id;
    }

    public List<Comment> getComments() {
        return comments;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setText(String text) {
        this.text = text;
    }

}
