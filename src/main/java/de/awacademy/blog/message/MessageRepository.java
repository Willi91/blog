package de.awacademy.blog.message;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MessageRepository extends CrudRepository<Message, Long> {

    List<Message> findAllByOrderByPostedAtDesc();

    Message findById(long id);

    Optional<Message> findMessageById(long id);

}