package de.awacademy.blog.Comments;

import javax.validation.constraints.Size;

public class CommentDTO {

    @Size(min = 1, max = 99)
    private String text;

    public CommentDTO(String text) {
        this.text = text;
    }

    public  String getText() {
        return text;

    }

}
