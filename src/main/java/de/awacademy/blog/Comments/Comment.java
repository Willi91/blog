package de.awacademy.blog.Comments;

import de.awacademy.blog.User.User;
import de.awacademy.blog.message.Message;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;

@Entity
public class Comment {


    @Id
    @GeneratedValue
    private long id;
    private String text;
    private Instant postedAt;

    @ManyToOne
    private User user;

    @ManyToOne
    private Message message;

    public Comment(){

    }

    public Comment(String text, Instant postedAt,Message message, User user) {
        this.text = text;
        this.postedAt = postedAt;
        this.message = message;
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Message getMessage() {
        return message;
    }


}









