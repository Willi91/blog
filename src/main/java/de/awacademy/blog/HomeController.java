package de.awacademy.blog;

import de.awacademy.blog.Comments.CommentRepository;
import de.awacademy.blog.User.User;
import de.awacademy.blog.message.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class HomeController {

    private MessageRepository messageRepository;
    private CommentRepository commentRepository;

    @Autowired
    public HomeController(MessageRepository messageRepository, CommentRepository commentRepository) {
        this.messageRepository = messageRepository;
        this.commentRepository = commentRepository;
    }

    @GetMapping("/")
    public String home(@ModelAttribute("sessionUser") User sessionUser, Model model) {
        model.addAttribute("messages", messageRepository.findAllByOrderByPostedAtDesc());
        model.addAttribute("comments", commentRepository.findAllByOrderByPostedAtDesc());
        return "home";
    }

}
